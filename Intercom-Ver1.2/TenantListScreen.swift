//
//  TenantListScreen.swift
//  Intercom-Ver1.2
//
//  Created by Alfredo Merino on 2/20/19.
//  Copyright © 2019 Alfredo Merino. All rights reserved.
//

import UIKit

class TenantListScreen: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var contacts: [Contact] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        contacts = createArray()
        
       

    }
    
    func createArray() -> [Contact] {
        var tempContacts: [Contact] = []
        
        let ivan = Contact(aptnum: 1, name: "I. Corchado")
        let nilson = Contact(aptnum: 2, name: "N.Nacimento")
        
        tempContacts.append(ivan)
        tempContacts.append(nilson)
        
        return tempContacts
    }
    
}

extension TenantListScreen: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contact = contacts[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        
       cell.setContact(contact: contact)
        
        return cell
    }
}
