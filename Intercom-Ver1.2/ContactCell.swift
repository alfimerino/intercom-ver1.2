//
//  ContactCell.swift
//  Intercom-Ver1.2
//
//  Created by Alfredo Merino on 2/20/19.
//  Copyright © 2019 Alfredo Merino. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {

    
    @IBOutlet weak var contactLabel: UILabel!
    
    
    func setContact(contact: Contact){

        contactLabel.text = contact.name
    }
}
