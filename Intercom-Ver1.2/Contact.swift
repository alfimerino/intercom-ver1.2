//
//  Contact.swift
//  Intercom-Ver1.2
//
//  Created by Alfredo Merino on 2/20/19.
//  Copyright © 2019 Alfredo Merino. All rights reserved.
//

import Foundation
import UIKit

class Contact {
    var aptNum: Int
    var name: String
    
    init(aptnum: Int, name: String) {
        self.aptNum = aptnum
        self.name = name
    }
}
